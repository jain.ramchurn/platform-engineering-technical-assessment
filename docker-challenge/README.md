# Code challenge

Using docker to run the following NodeJS application and expose it on port 8080

# Prequisites

Before getting started, make sure you have docker-buildx installed on your system.

## Deployment

Follow the steps below to build the Docker image and run the application:

1. Build the Docker image:
```
DOCKER_BUILDKIT=1 docker build -t webapp .
```

2. Run a container using the built image:
```
docker run -d --name app -p 8080:8080 -t webapp
```

Run curl locally on port 8080.
Access the application on `http://127.0.0.1:8080` using `curl`. 

```
curl http://127.0.0.1:8080
```

## Cleanup

Stopping the container `app`.
```
docker container stop app 
```
Removing the container `app`.
```
docker container rm app 
```
Removing the built image `webapp`.
```
docker image rm webapp
```
