from flask import Flask, jsonify, request
import json, subprocess

app = Flask(__name__)

@app.route('/system', methods=['GET'])
def get_system_info():
    server_load = calculate_server_load()
    disk_space = calculate_disk_space()

    return jsonify({'server_load': server_load, 'disk_space': disk_space})

@app.route('/return_value', methods=['GET'])
def get_return_value():
    with open('tech_assess.json') as file:
        data = json.load(file)
        return_value = data.get('tech', {}).get('return_value')

    return jsonify({'return_value': return_value})

@app.route('/return_value', methods=['POST'])
def update_return_value():
    new_return_value = request.json.get('return_value')

    with open('tech_assess.json') as file:
        data = json.load(file)
        print(data)
        data['tech']['return_value'] = new_return_value

    with open('tech_assess.json', 'w') as file:
        json.dump(data, file)

    return jsonify({'message': 'return_value updated successfully'})

def calculate_server_load():
    output = subprocess.check_output(['uptime']).decode().strip()
    load_average = output.split(':')[-1].split(',')
    return load_average

def calculate_disk_space():
    output = subprocess.check_output(['df', '-h']).decode().strip()
    lines = output.split('\n')
    disk_info = lines[1].split()
    available_space = disk_info[3]
    return available_space

if __name__ == '__main__':
    app.run()
