1. Install the required dependencies
```bash
pip install Flask
```

2. Run the flask server
```bash
python3  codechallenge.py
```
The server will start running on `http://localhost:5000`

3. Use the following endpoints to interact with the API:
* `GET /system`: return the server load and disk space information in JSON format.
* `GET /return_value`: return the current return value from the `tech_assess.json` file.
* `POST /return_value`: Updates the return value in the `tech_assess.json` file. Send a JSON payload with the new return value

```json
{
    "return_value" : "new_value"
}
```

4. Use an HTTP client like Postman to test the API endpoints.
