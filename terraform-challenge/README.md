# Terraform Challenge
 Create a bastion host in Terraform within AWS.

## Requirements:
* EC2 instance
* Security Group
* Ports: Inbound 22 | Outbound all traffic
* Any Linux AMI of your choice.
* Instance type: t3.nano
* IAM role attached to EC2

## Variables 

| Name | Description |
|------|-------------|
|subnetid| VPC Subnet Id to launch in |
|ec2_security_groups|List of security group names to associate with.|
|keyname|Key name of the Key Pair to use for the instance|
|ami|AMI to use for the instance|
|instance_type |Instance type to use for the instance. (defeault is t3.micro) |
|security_group | Id of security group to modify ports|

## Modules

## Requirements

No requirements.

## Modules

### Instance-profile

Create IAM policy and IAM Role, attach the IAM policy to the IAM role and create an IAM instance profile. 

#### Resources

| Name | Type |
|------|------|
| [aws_iam_instance_profile.ec2_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_policy.ec2_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy_attachment.attach_ec2_policy_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy_attachment) | resource |
| [aws_iam_role.ec2_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |

#### Inputs

No inputs.

#### Outputs

| Name | Description |
|------|-------------|
| <a name="output_ec2_profile_name"></a> [ec2\_profile\_name](#output\_ec2\_profile\_name) | n/a |

---

## Note
The terraform script uses the default VPC.
