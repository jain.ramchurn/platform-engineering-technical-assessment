module "ec2_instance_profile" {
  source = "./modules/instance-profiles"
}

resource "aws_vpc_security_group_ingress_rule" "bastion" {
  security_group_id = var.security_group

  cidr_ipv4   = "0.0.0.0/0"
  from_port   = 22
  ip_protocol = "tcp"
  to_port     = 22
}

resource "aws_vpc_security_group_egress_rule" "bastion" {
  security_group_id = var.security_group

  cidr_ipv4   = "0.0.0.0/0"
  from_port   = -1
  ip_protocol = "-1"
  to_port     = -1
}

resource "aws_instance" "bastion" {
  depends_on = [
    module.ec2_instance_profile
  ]
  ami                         = var.ami
  instance_type               = var.instance_type
  security_groups             = var.ec2_security_groups
  subnet_id                   = var.subnetid
  associate_public_ip_address = true
  key_name                    = var.keyname
  iam_instance_profile        = module.ec2_instance_profile.ec2_profile_name
}

