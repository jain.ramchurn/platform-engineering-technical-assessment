variable "vpc" {
  type = string
}

variable "subnetid" {
  type = string
}

variable "security_group" {
  type = string
}

variable "ec2_security_groups" {
  type = list(string)
}

variable "keyname" {
  type = string
}

variable "ami" {
  type = string
}

variable "instance_type" {
  type    = string
  default = "t3.micro"
}

